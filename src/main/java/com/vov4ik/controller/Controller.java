package com.vov4ik.controller;

import com.vov4ik.model.AnnoAndReflection;
import com.vov4ik.model.InfoClass;
import com.vov4ik.model.User;

public class Controller {
    AnnoAndReflection annoAndReflection = new AnnoAndReflection();

    public String ShowFieldsWithAnnotation(Object object) {
        return annoAndReflection.showAnnotatedFields(object)
                + annoAndReflection.showAnnotatedMethods(object);
    }

    public String ShowInfoOfAnnotation(Object object) {
        return annoAndReflection.showAnnotationInfo(object);
    }

    public String someMethodsInvoke(User user) {
        return annoAndReflection.invokeDifferentMethods(user);
    }

    public String myMethodsInvoke(User user) {
        return annoAndReflection.invokeMyMethods(user);
    }

    public String showInputClassInfo(Object object) {
        InfoClass infoClass = new InfoClass(object);
        return ("Information about input Class:\n" + infoClass.showInputClassConstructors()
                + infoClass.showInputClassFields() + infoClass.showInputClassMethods());
    }
}
