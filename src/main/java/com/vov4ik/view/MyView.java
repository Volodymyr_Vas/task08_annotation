package com.vov4ik.view;

import com.vov4ik.controller.Controller;
import com.vov4ik.model.User;
import com.vov4ik.view.logger.MyLogger;

import java.util.*;

public class MyView {
    private User user;
    private User user1;
    private MyLogger logger;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scanner;

    public MyView() {
        user = new User();
        user1 = new User("Dambo", "Kyiv", 12);
        logger = new MyLogger();
        controller = new Controller();
        scanner = new Scanner(System.in);
        setMenu();
        setMethodsMenu();
    }

    private void setMenu () {
        menu = new LinkedHashMap<>();
        menu.put("1", "Show annotated fields");
        menu.put("2", "Show annotation info");
        menu.put("3", "Invoke 2 different methods");
        menu.put("4", "Invoke 2 myMethods");
        menu.put("5", "Show information about Class");
        menu.put("Q", "Quit");
    }

    private void setMethodsMenu(){
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showAnnotatedFields);
        methodsMenu.put("2", this::showAnnotationInfo);
        methodsMenu.put("3", this::invokeDifferentMethods);
        methodsMenu.put("4", this::invokeTwoMyMethods);
        methodsMenu.put("5", this::showInfoAboutClass);
    }

    private void showAnnotatedFields() {
        logger.printInfo(controller.ShowFieldsWithAnnotation(user));
    }

    private void showAnnotationInfo() {
        logger.printInfo(controller.ShowInfoOfAnnotation(user1));
    }

    private void invokeDifferentMethods() {
        logger.printInfo(controller.someMethodsInvoke(user1));
    }

    private void invokeTwoMyMethods() {
        logger.printInfo(controller.myMethodsInvoke(user));
    }

    private void showInfoAboutClass() {
        logger.printInfo(controller.showInputClassInfo(user));
    }

    public void showMenu() {
        String input;
        do {
            logger.printInfo("Menu:\n");
            for (String key : menu.keySet()) {
                logger.printInfo(key + " - " + menu.get(key));
            }
            logger.printInfo("Please, select one of points");
            input = scanner.nextLine().toUpperCase();
            if (input.equals("Q")) {
                break;
            }else try {
                methodsMenu.get(input).print();
            } catch (Exception e) {
                logger.printWarning("Your input was incorrect. \nPlease, try again");
            }
        } while (!input.equals("Q"));
    }
}
