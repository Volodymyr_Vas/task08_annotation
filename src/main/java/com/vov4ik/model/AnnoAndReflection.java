package com.vov4ik.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class AnnoAndReflection {

    public String showAnnotatedFields(Object obj) {
        StringBuilder stringBuilder = new StringBuilder();
        List<Field> annotatedFields = getAnnotatedFields(obj);
        for (Field annotatedField : annotatedFields) {
            try {
                annotatedField.setAccessible(true);
                stringBuilder.append("\nField: ").append(annotatedField.getType().getSimpleName())
                        .append(' ').append(annotatedField.getName())
                        .append(" = ").append(annotatedField.get(obj));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return stringBuilder.toString();
    }

    public String showAnnotatedMethods(Object obj) {
        StringBuilder stringBuilder = new StringBuilder();
        List<Method> annotatedMethods = getAnnotatedMethods(obj);
        for (Method method : annotatedMethods) {
            method.setAccessible(true);
            stringBuilder.append("\nMethod: ").append(method.getReturnType().getSimpleName())
                    .append(' ').append(method.getName())
                    .append('(');
            Arrays.stream(method.getParameterTypes()).forEach(s -> stringBuilder.append(s.getSimpleName()));
            stringBuilder.append(")");
        }
        return stringBuilder.toString();
    }

    private List<Field> getAnnotatedFields(Object obj) {
        Field[] allFields = obj.getClass().getDeclaredFields();
        List<Field> annotatedFields = new LinkedList<>();
        for (Field field : allFields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                annotatedFields.add(field);
            }
        }
        return annotatedFields;
    }

    private List<Method> getAnnotatedMethods(Object obj) {
        Method[] allMethods = obj.getClass().getDeclaredMethods();
        List<Method> annotatedMethods = new LinkedList<>();
        for (Method method : allMethods) {
            if (method.isAnnotationPresent(MyAnnotation.class)) {
                annotatedMethods.add(method);
            }
        }
        return annotatedMethods;
    }

    public String showAnnotationInfo(Object obj) {
        StringBuilder stringBuilder = new StringBuilder();
        List<Field> annotatedFields = getAnnotatedFields(obj);
        List<Method> annotatedMethods = getAnnotatedMethods(obj);
        for (Field annotatedField : annotatedFields) {
            MyAnnotation myAnnotation = annotatedField.getAnnotation(MyAnnotation.class);
            stringBuilder.append("\nAnnotation: ").append('@')
                    .append(myAnnotation.annotationType().getSimpleName())
                    .append("( Name: ").append(myAnnotation.name()).append(" , Age: ")
                    .append(myAnnotation.age()).append(')');
        }
        for (Method method : annotatedMethods) {
            MyAnnotation myAnnotation = method.getAnnotation(MyAnnotation.class);
            stringBuilder.append("\nAnnotation: ").append('@')
                    .append(myAnnotation.annotationType().getSimpleName())
                    .append("( Name: ").append(myAnnotation.name()).append(" , Age: ")
                    .append(myAnnotation.age()).append(')');
        }
        return stringBuilder.toString();
    }

    public String invokeDifferentMethods(User user) {
        String firstMethodResult = null;
        int secondMethodResult = -1;
        try {
            Method firstMethod = user.getClass().getDeclaredMethod("getUserInfo", String.class);
            Method secondMethod = user.getClass().getDeclaredMethod("uselessMethod", int.class);
            firstMethod.setAccessible(true);
            secondMethod.setAccessible(true);
            firstMethodResult = (String) firstMethod.invoke(user, "Invoking getUserInfo");
            secondMethodResult = (int) secondMethod.invoke(user, 25);
        } catch (NoSuchMethodException | InvocationTargetException
                | IllegalAccessException e) {
            e.printStackTrace();
        }
        return ("First method:\n" + firstMethodResult
                + "; \nSecond method: " + secondMethodResult);
    }

    public String invokeMyMethods(User user) {
        String firstMyMethodResult = null;
        String secondMyMethodResult = null;
        try {
            Method firstMyMethod = user.getClass().getDeclaredMethod("myMethod", String[].class);
            Method secondMyMethod = user.getClass().getDeclaredMethod("myMethod", String.class, int[].class);
            firstMyMethodResult = (String) firstMyMethod.invoke(user,
                    new Object[]{new String[]{"wasd", "acab", "qwerty"}});
            secondMyMethodResult = (String) secondMyMethod.invoke(user, "SOAD", new int[]{100, 200, 300});
        } catch (NoSuchMethodException | IllegalAccessException
                | InvocationTargetException e) {
            e.printStackTrace();
        }
        return ("Results:\n" + firstMyMethodResult + '\n' + secondMyMethodResult);
    }
}
