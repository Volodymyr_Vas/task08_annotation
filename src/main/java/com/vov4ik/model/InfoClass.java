package com.vov4ik.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class InfoClass {
    private Class aClass;

    public <T> InfoClass(T obj) {
        aClass = obj.getClass();
    }

    public String showInputClassConstructors() {
        Constructor[] constructors = aClass.getConstructors();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Constructors:\n");
        for(Constructor constructor : constructors) {
            stringBuilder.append(Modifier.toString(constructor.getModifiers()))
                    .append(' ').append(aClass.getSimpleName()).append('(')
                    .append(Arrays.toString(constructor.getParameters())).append(")\n");
        }
        return stringBuilder.toString();
    }

    public String showInputClassFields() {
        Field[] fields = aClass.getDeclaredFields();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Fields:\n");
        for (Field field : fields) {
            stringBuilder.append(Modifier.toString(field.getModifiers()))
                    .append(' ').append(field.getType().getSimpleName())
                    .append(' ').append(field.getName()).append('\n');
        }
        return stringBuilder.toString();
    }

    public String showInputClassMethods() {
        Method[] methods = aClass.getDeclaredMethods();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Methods:\n");
        for (Method method : methods) {
            stringBuilder.append(Modifier.toString(method.getModifiers()))
                    .append(' ').append(method.getReturnType().getSimpleName())
                    .append(' ').append(method.getName()).append('(')
                    .append(Arrays.toString(method.getParameters()))
                    .append(")\n");
        }
        return stringBuilder.toString();
    }
}
