package com.vov4ik.model;

public class User {
    @MyAnnotation(name = "Ostap Drozdov", age = 88)
    private String name;
    private String address;
    @MyAnnotation(age = 256)
    private int age;

    public User(String name, String address, int age) {
        this.name = name;
        this.address = address;
        this.age = age;
    }

    public User() {
        name = "Vov4ik";
        age = 24;
        address = "Lviv, Ukraine";
    }

    @MyAnnotation(name = "vov4ik", age = 24)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @MyAnnotation(name = "Not_My_Name", age = 88)
    public String getUserInfo(String asker){
        String userInfo = "name: " + name
                + "\nage: " + age
                + "\naddress: " + address
                + "\nasker: " + asker;
        return userInfo;
    }

    private int uselessMethod(int someInt){
        int uselessInt = someInt + age/2 + someInt*2 + 1;
        return uselessInt;
    }

    public String myMethod(String... someArgs){
        return "This method takes only Strings";
    }

    public String myMethod(String oneString, int... someInts){
        return "this method takes String " + oneString
                + " and ints " + someInts[0];
    }
}
